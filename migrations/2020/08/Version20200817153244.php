<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200817153244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'staff.overview', 'hash' => 'd70fc237d6b14aa4b9f4bcc794fc7bc3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zaměstnanci', 'plural1' => '', 'plural2' => ''],
            ['original' => 'staff.overview.title', 'hash' => '21050cb5426d4077ee3804fdf757379f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zaměstnanci|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.staff-type', 'hash' => '8150d3b8d322d27c405055040b85606b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.is-active', 'hash' => '7420af55919136e21fc7075517dd0149', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.action.staff-type', 'hash' => '221e1c1743c35a763317cf277996f729', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.action.new', 'hash' => '5f3b75adf38f308060f8236010491f0f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit zaměstnance', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.name', 'hash' => '08b16d1dabded61dc0388e281a2e462d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.user-front.contact', 'hash' => '8f19c07f080666562eb9a940e05ac347', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kontakt', 'plural1' => '', 'plural2' => ''],
            ['original' => 'staff.overview-type.title', 'hash' => 'd616ca1d2421c5a9d5b1ee8eeea4fbbb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělení|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.action.staff', 'hash' => '41cfb8ca604fce5e5c1439a3741363ce', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled zaměstnanců', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview-type.action.new', 'hash' => '320ab60dbeb3e971fb924670cd2bc78c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview-type.name', 'hash' => '793ff0e0f77a60bc40f15cb3b4b5e79a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview-type.action.edit', 'hash' => 'd929ed4c62bf7c2ad058527392983b61', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'staff-type.edit.title - %s', 'hash' => '798224b76f7de84bf19687cdbf786325', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělení %s|Editace', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.name', 'hash' => '62c59ee27a620f7d2dc40a69ebeba00e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.name.req', 'hash' => '73958e989046cdb1ba18a204ea513a41', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.content', 'hash' => 'f833c4ccc9fb58155f64c34b8775d595', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.send', 'hash' => 'fd3757fbf3ce3b2b79c5c45cb930e6d9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.send-back', 'hash' => 'f9c1ae553896d2ea9797a38801c0c7c3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.back', 'hash' => '110fd997507b156d09122f363566ff75', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'staff-type.edit.title', 'hash' => 'a58407baa0dd6d53561cfa446611e328', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.flash.success.create', 'hash' => '0684c2b349fbfd0f878a708f67a76e23', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělení bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff-type.edit.flash.success.update', 'hash' => '4c080d6a97198f33c942140e3c406f41', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělení bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'staff.edit.title', 'hash' => '5bdf3ad58ba6fcae898770c331b769f3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení zaměstnance', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.staff-type-id', 'hash' => '5eb66131a82a5c181f6e374b84025556', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.staff-type-id.req', 'hash' => 'f115de0e7fa5d969c18d1c8ca939c03b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.name', 'hash' => 'd2ed5b61513ab95bd49eb77ce2f1e450', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.name.req', 'hash' => 'befc77354d1db6f129836b1627cd2a45', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.surname', 'hash' => '86d8713b2b52928b917d659c62c3076b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příjmení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.surname.req', 'hash' => '57b9e5568c472e8a12d4addb8bd1d32c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím příjmení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.nickname', 'hash' => 'c7f765152b075646f1230182dc04aaa2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přezdívka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.function', 'hash' => '701497efc396026553df45ca104afae3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Funkce', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.email', 'hash' => '7942345949a3ab06f3702118ab641dda', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.phone', 'hash' => '60b5480126ee75e9c04c4fee50dd1464', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Telefon', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.image-preview', 'hash' => 'de685be5e99da330dbf7b613c8c43611', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Profilový obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.image-preview.rule-image', 'hash' => '49ed8b4440e8a0c3c6cfe46b7522deed', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.is-active', 'hash' => 'ad5951720d0e85cbb933288b7fb35904', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.send', 'hash' => '12b9d8f9a8ef3876249e54c158cc8c7f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.send-back', 'hash' => 'a27b5a5e8b3d6d8aa14734ccafde9b63', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.back', 'hash' => 'b1d094bad03232a9353413c3652fde34', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'staff.edit.title - %s', 'hash' => '8ce808dbb9e96f8a849a10303658d21b', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace zaměstnance', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.flash.success.create', 'hash' => '58f527ad6e616df25863f10723d48f17', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zaměstnanec byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.staff.edit.flash.success.update', 'hash' => '12153d3f38e25a9769130b54868381f5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zaměstnanec byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.staff.title', 'hash' => '3da1fa84f7510640a3adf886231dfd37', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zaměstnanci', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.staff.description', 'hash' => 'caea967e458b561eb56d57e0e921b724', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat zaměstnance a jejich oddělení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview.action.edit', 'hash' => 'f003091f3cb9b24248dd7a106966add5', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.staff.overview-type.action.flash.sort.success', 'hash' => '4e17e0ecc390eec7a964379122f8ecda', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí oddělení bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
