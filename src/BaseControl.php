<?php

declare(strict_types=1);

namespace Skadmin\Staff;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'staff';
    public const DIR_IMAGE = 'staff';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-users']),
            'items'   => ['overview'],
        ]);
    }
}
