<?php

declare(strict_types=1);

namespace Skadmin\Staff\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Staff\BaseControl;
use Skadmin\Staff\Doctrine\Staff\Staff;
use Skadmin\Staff\Doctrine\Staff\StaffFacade;
use Skadmin\Staff\Doctrine\StaffType\StaffTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory   $webLoader;
    private StaffFacade     $facade;
    private StaffTypeFacade $facadeStaffType;
    private Staff           $staff;
    private ImageStorage    $imageStorage;

    public function __construct(?int $id, StaffFacade $facade, StaffTypeFacade $facadeStaffType, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade          = $facade;
        $this->facadeStaffType = $facadeStaffType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->staff = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->staff->isLoaded()) {
            return new SimpleTranslation('staff.edit.title - %s', $this->staff->getFullName());
        }

        return 'staff.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->staff->isLoaded()) {
            if ($identifier !== null && $this->staff->getImagePreview() !== null) {
                $this->imageStorage->delete($this->staff->getImagePreview());
            }

            $staff = $this->facade->update(
                $this->staff->getId(),
                $values->name,
                $values->surname,
                $values->nickname,
                $values->function,
                $values->email,
                $values->phone,
                $values->facebook,
                $values->isActive,
                $this->facadeStaffType->get($values->staffTypeId),
                $identifier
            );
            $this->onFlashmessage('form.staff.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $staff = $this->facade->create(
                $values->name,
                $values->surname,
                $values->nickname,
                $values->function,
                $values->email,
                $values->phone,
                $values->facebook,
                $values->isActive,
                $this->facadeStaffType->get($values->staffTypeId),
                $identifier
            );
            $this->onFlashmessage('form.staff.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $staff->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->staff = $this->staff;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $datastaffType = $this->facadeStaffType->getPairs('id', 'name');

        // INPUT
        $form->addText('name', 'form.staff.edit.name')
            ->setRequired('form.staff.edit.name.req');
        $form->addText('surname', 'form.staff.edit.surname')
            ->setRequired('form.staff.edit.surname.req');

        $form->addText('nickname', 'form.staff.edit.nickname');

        $form->addEmail('email', 'form.staff.edit.email');
        $form->addText('phone', 'form.staff.edit.phone');
        $form->addUrl('facebook', 'form.staff.edit.facebook', 'facebook.com');

        $form->addText('function', 'form.staff.edit.function');

        $form->addImageWithRFM('imagePreview', 'form.staff.edit.image-preview');

        // STAFF
        $form->addSelect('staffTypeId', 'form.staff.edit.staff-type-id', $datastaffType)
            ->setRequired('form.staff.edit.staff-type-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // CHECKBOX
        $form->addCheckbox('isActive', 'form.staff.edit.is-active')
            ->setDefaultValue(true);

        // BUTTON
        $form->addSubmit('send', 'form.staff.edit.send');
        $form->addSubmit('sendBack', 'form.staff.edit.send-back');
        $form->addSubmit('back', 'form.staff.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->staff->isLoaded()) {
            return [];
        }

        return [
            'name'        => $this->staff->getName(),
            'surname'     => $this->staff->getSurname(),
            'nickname'    => $this->staff->getNickname(),
            'email'       => $this->staff->getEmail(),
            'phone'       => $this->staff->getPhone(),
            'facebook'    => $this->staff->getFacebook(),
            'function'    => $this->staff->getFunction(),
            'isActive'    => $this->staff->isActive(),
            'staffTypeId' => $this->staff->getStaffType()->getId(),
        ];
    }
}
