<?php

declare(strict_types=1);

namespace Skadmin\Staff\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Staff\BaseControl;
use Skadmin\Staff\Doctrine\StaffType\StaffType;
use Skadmin\Staff\Doctrine\StaffType\StaffTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory   $webLoader;
    private StaffTypeFacade $facade;
    private StaffType       $staffType;

    public function __construct(?int $id, StaffTypeFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->staffType = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->staffType->isLoaded()) {
            return new SimpleTranslation('staff-type.edit.title - %s', $this->staffType->getName());
        }

        return 'staff-type.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->staffType->isLoaded()) {
            $staffType = $this->facade->update(
                $this->staffType->getId(),
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.staff-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $staffType = $this->facade->create(
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.staff-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $staffType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.staff-type.edit.name')
            ->setRequired('form.staff-type.edit.name.req');
        $form->addTextArea('content', 'form.staff-type.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.staff-type.edit.send');
        $form->addSubmit('sendBack', 'form.staff-type.edit.send-back');
        $form->addSubmit('back', 'form.staff-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->staffType->isLoaded()) {
            return [];
        }

        return [
            'name'    => $this->staffType->getName(),
            'content' => $this->staffType->getContent(),
        ];
    }
}
