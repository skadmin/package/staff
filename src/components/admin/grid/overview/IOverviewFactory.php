<?php

declare(strict_types=1);

namespace Skadmin\Staff\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
