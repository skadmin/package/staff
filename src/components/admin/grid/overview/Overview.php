<?php

declare(strict_types=1);

namespace Skadmin\Staff\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Staff\BaseControl;
use Skadmin\Staff\Doctrine\Staff\Staff;
use Skadmin\Staff\Doctrine\Staff\StaffFacade;
use Skadmin\Staff\Doctrine\StaffType\StaffTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function implode;
use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private StaffFacade     $facade;
    private LoaderFactory   $webLoader;
    private StaffTypeFacade $facadeStaffType;
    private ImageStorage    $imageStorage;

    public function __construct(StaffFacade $facade, StaffTypeFacade $facadeStaffType, Translator $translator, User $user, ImageStorage $imageStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade          = $facade;
        $this->facadeStaffType = $facadeStaffType;
        $this->imageStorage    = $imageStorage;
        $this->webLoader       = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'staff.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataStaffType = $this->facadeStaffType->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC')
            ->addOrderBy('a.name', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Staff $staff): ?Html {
                if ($staff->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$staff->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.staff.overview.name')
            ->setRenderer(function (Staff $staff): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $staff->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $render = new Html();
                $name->setText($staff->getFullName());

                $render->addHtml($name);
                if ($staff->getFunction() !== '') {
                    $function = Html::el('code', ['class' => 'text-muted small'])->setText($staff->getFunction());

                    $render->addHtml('<br/>')
                        ->addHtml($function);
                }

                return $render;
            });
        $grid->addColumnText('contact', 'grid.user-front.contact')
            ->setRenderer(static function (Staff $staff): Html {
                $contact = new Html();

                $contacts = [];
                if ($staff->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($staff->getEmail());
                }

                if ($staff->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($staff->getPhone());
                }

                if ($staff->getFacebook() !== '') {
                    $contacts[] = Utils::createFacebookContact($staff->getFacebook(), $staff->getFullName());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        $grid->addColumnText('staffType', 'grid.staff.overview.staff-type', 'staffType.name');
        $this->addColumnIsActive($grid, 'staff.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.staff.overview.name', ['name', 'surname', 'nickname']);
        $grid->addFilterSelect('staffType', 'grid.staff.overview.staff-type', $dataStaffType, 'staffType')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'staff.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.staff.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.staff.overview.action.staff-type', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.staff.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.staff.overview-type.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
