<?php

declare(strict_types=1);

namespace Skadmin\Staff\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}
