<?php

declare(strict_types=1);

namespace Skadmin\Staff\Doctrine\Staff;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Staff\Doctrine\StaffType\StaffType;
use SkadminUtils\DoctrineTraits\Entity;

use function sprintf;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Staff
{
    use Entity\Id;
    use Entity\HumanName {
        getFullName as getTraitFullName;
    }
    use Entity\Nickname;
    use Entity\Contact;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Created;
    use Entity\Facebook;

    #[ORM\Column]
    private string $function = '';

    #[ORM\ManyToOne(targetEntity: StaffType::class, inversedBy: 'staff')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private StaffType $staffType;

    public function update(string $name, string $surname, string $nickname, string $function, string $email, string $phone, string $facebook, bool $isActive, StaffType $staffType, ?string $imagePreview): void
    {
        $this->name     = $name;
        $this->surname  = $surname;
        $this->nickname = $nickname;
        $this->function = $function;
        $this->email    = $email;
        $this->phone    = $phone;
        $this->facebook = $facebook;
        $this->isActive = $isActive;

        $this->staffType = $staffType;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getFunction(): string
    {
        return $this->function;
    }

    public function getStaffType(): StaffType
    {
        return $this->staffType;
    }

    /**
     * First "name" OR with $reverse[true] first "surname"
     */
    public function getFullName(bool $reverse = false): string
    {
        if ($this->getNickname() === '') {
            return $this->getTraitFullName($reverse);
        }

        if ($reverse) {
            return sprintf('%s "%s" %s', $this->getSurname(), $this->getNickname(), $this->getName());
        }

        return sprintf('%s "%s" %s', $this->getName(), $this->getNickname(), $this->getSurname());
    }
}
