<?php

declare(strict_types=1);

namespace Skadmin\Staff\Doctrine\Staff;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Staff\Doctrine\StaffType\StaffType;
use SkadminUtils\DoctrineTraits\Facade;

final class StaffFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Staff::class;
    }

    public function create(string $name, string $surname, string $nickname, string $function, string $email, string $phone, string $facebook, bool $isActive, StaffType $staffType, ?string $imagePreview): Staff
    {
        return $this->update(null, $name, $surname, $nickname, $function, $email, $phone, $facebook, $isActive, $staffType, $imagePreview);
    }

    public function update(?int $id, string $name, string $surname, string $nickname, string $function, string $email, string $phone, string $facebook, bool $isActive, StaffType $staffType, ?string $imagePreview): Staff
    {
        $staff = $this->get($id);
        $staff->update($name, $surname, $nickname, $function, $email, $phone, $facebook, $isActive, $staffType, $imagePreview);

        if (! $staff->isLoaded()) {
            $staff->setSequence($this->getValidSequence());
        }

        $this->em->persist($staff);
        $this->em->flush();

        return $staff;
    }

    public function get(?int $id = null): Staff
    {
        if ($id === null) {
            return new Staff();
        }

        $staff = parent::get($id);

        if ($staff === null) {
            return new Staff();
        }

        return $staff;
    }
}
