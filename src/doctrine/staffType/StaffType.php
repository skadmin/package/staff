<?php

declare(strict_types=1);

namespace Skadmin\Staff\Doctrine\StaffType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Staff\Doctrine\Staff\Staff;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class StaffType
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    /** @var ArrayCollection|Collection|Staff[] */
    #[ORM\OneToMany(targetEntity: Staff::class, mappedBy: 'staffType')]
    #[ORM\OrderBy(['sequence' => 'ASC', 'name' => 'ASC'])]
    private ArrayCollection|Collection|array $staff;

    public function __construct()
    {
        $this->staff = new ArrayCollection();
    }

    public function update(string $name, string $content): void
    {
        $this->name    = $name;
        $this->content = $content;
    }

    /**
     * @return ArrayCollection|Collection|Staff[]
     */
    public function getstaff(bool $onlyActive = false): ArrayCollection|Collection|array
    {
        if (! $onlyActive) {
            return $this->staff;
        }

        $criteria = Criteria::create()->where(Criteria::expr()->eq('isActive', true));

        return $this->staff->matching($criteria);
    }
}
