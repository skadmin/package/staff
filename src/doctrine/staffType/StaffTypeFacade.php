<?php

declare(strict_types=1);

namespace Skadmin\Staff\Doctrine\StaffType;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function count;

final class StaffTypeFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = StaffType::class;
    }

    public function create(string $name, string $content): StaffType
    {
        return $this->update(null, $name, $content);
    }

    public function update(?int $id, string $name, string $content): StaffType
    {
        $staffType = $this->get($id);
        $staffType->update($name, $content);

        if (! $staffType->isLoaded()) {
            $staffType->setWebalize($this->getValidWebalize($name));
            $staffType->setSequence($this->getValidSequence());
        }

        $this->em->persist($staffType);
        $this->em->flush();

        return $staffType;
    }

    public function get(?int $id = null): StaffType
    {
        if ($id === null) {
            return new StaffType();
        }

        $staffType = parent::get($id);

        if ($staffType === null) {
            return new StaffType();
        }

        return $staffType;
    }

    /**
     * @param array<int> $ids
     *
     * @return StaffType[]
     */
    public function findByIds(array $ids): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        if (count($ids) > 0) {
            $criteria['id'] = $ids;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @return StaffType[]
     */
    public function getAll(): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
